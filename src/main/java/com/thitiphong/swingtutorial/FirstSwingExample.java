package com.thitiphong.swingtutorial;

import javax.swing.JButton;
import javax.swing.JFrame;

public class FirstSwingExample {
   public static void main(String[] args) {
      JFrame frame = new JFrame();
      JButton button = new JButton("Click");
      button.setBounds(130, 100, 100, 40);
      frame.setLayout(null);
      frame.add(button);
      frame.setTitle("First JFrame");
      frame.setSize(400, 500);
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.setVisible(true);
   }

}
