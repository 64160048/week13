package com.thitiphong.swingtutorial;

import javax.swing.*;
class MyApp {
    JFrame frame ;
    JButton button ;
    public MyApp() {
        frame = new JFrame();
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        frame.setLayout(null);
        frame.add(button);
        frame.setTitle("First JFrame");
        frame.setSize(400, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}

public class Simple {
    public static void main(String[] args) {
        MyApp app = new MyApp();
     }
  
}
